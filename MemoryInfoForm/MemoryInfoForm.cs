﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;

namespace CYFang
{
    public partial class MemoryInfoForm : Form
    {
        private Timer timer = new Timer();

        public MemoryInfoForm()
        {
            InitializeComponent();
            timer.Tick += Timer_Tick;
            timer.Interval = 1000;
            timer.Start();
        }

        private void Timer_Tick(object sender, System.EventArgs e)
        {
            var process = Process.GetProcesses().OrderByDescending(p => p.PrivateMemorySize64);
            listBox1.Items.Clear();

            foreach (var p in process)
                listBox1.Items.Add(String.Format("{0} 使用實體記憶體為{1}MB"
                    , p.ProcessName, p.PrivateMemorySize64 / 1024 / 1024));
        }

    }
}
